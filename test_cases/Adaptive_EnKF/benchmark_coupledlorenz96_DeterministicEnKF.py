#!/usr/bin/env python
# Wrapper that calls "coupledlorenz96_enkf_test_driver" to Perform EnKF with multiple inflation factors and localization radii

import sys
import os
import numpy as np

sys.path.insert(1, os.path.abspath('../../'))

DATES_ROOT_PATH = os.environ.get("DATES_ROOT_PATH")
if DATES_ROOT_PATH is None:
    try:
        import dates_setup
        dates_setup.initialize_dates()
        DATES_ROOT_PATH = os.environ.get("DATES_ROOT_PATH")
    except:
        DATES_ROOT_PATH = os.path.getcwd()

res_dir = os.path.join(DATES_ROOT_PATH , 'Results/CoupledLorenz_OED_DeterministicEnKF')

overwrite = False

inflation_factors = np.arange(1., 3., 0.1)
localization_radii = np.append(np.arange(0.1, 4, 0.2), np.infty)

it = 1
num_experiments = inflation_factors.size * localization_radii.size

for infl in inflation_factors:
    for loc in localization_radii:
        cmd = "%s coupledlorenz96_DeterministicEnKF_test_driver.py %f %f %s %s" % (sys.executable, infl, loc, res_dir, overwrite)
        print("\n%s\nITERATION: %d / %d \n%s\nRUNNING 'coupledlorenz96_DeterministicEnKF_test_driver.py'\n\tWITH INFLATION FACTOR %f AND LOCALIZATION PARAMETER %f" % (('*'*40), it,
                                                                                                                                                                       num_experiments, ('*'*40), infl, loc))
        os.system(cmd)
        it += 1

