
#
# ########################################################################################## #
#                                                                                            #
#   DATeS: Data Assimilation Testing Suite.                                                  #
#                                                                                            #
#   Copyright (C) 2016  A. Sandu, A. Attia, P. Tranquilli, S.R. Glandon,                     #
#   M. Narayanamurthi, A. Sarshar, Computational Science Laboratory (CSL), Virginia Tech.    #
#                                                                                            #
#   Website: http://csl.cs.vt.edu/                                                           #
#   Phone: 540-231-6186                                                                      #
#                                                                                            #
#   This program is subject to the terms of the Virginia Tech Non-Commercial/Commercial      #
#   License. Using the software constitutes an implicit agreement with the terms of the      #
#   license. You should have received a copy of the Virginia Tech Non-Commercial License     #
#   with this program; if not, please contact the computational Science Laboratory to        #
#   obtain it.                                                                               #
#                                                                                            #
# ########################################################################################## #
#

import os
import subprocess


def __test_python_modules(ignore_non_essential=True, silent=False):
    """
    Test all Python modules those should be imported
    """
    __splt = "*" * 50
    # 1- Numpy (Essential)
    try:
        import numpy as np
    except(ImportError):
        print("%s\n \rFailed to import numpy module!" % (__splt))
        print("To use DATeS, you MUST have Numpy installed on your machine ")
        print(__splt)
        raise

    # 2- Scipy (Essential)
    try:
        import scipy
    except(ImportError):
        print("%s\n \rFailed to import scipy module!" % (__splt))
        print("To use DATeS, you MUST have Scipy installed on your machine ")
        print(__splt)
        raise

    # 3- Matplotlib (Non-Essential) WARNING
    try:
        import matplotlib
    except(ImportError):
        if ignore_non_essential:
            if silent:
                pass
            else:
                print("%s\n \rFailed to import matplotlib module!" % (__splt))
                print("To plot DATeS results, you may want to consider installing matplotlib module")
                print(__splt)
        else:
            print("%s\n \rFailed to import matplotlib module!" % (__splt))
            print("To properly use all DATeS modules, please install matplotlib\n%s\n" % (__splt))
            raise

    # 5- Scikit-Learn (Essential for GMM) WARNING
    try:
        import sklearn
    except(ImportError):
        if ignore_non_essential:
            if silent:
                pass
            else:
                print("%s\n \rFailed to import sklearn (SciKit-Learn) module!" % (__splt))
                print("Some modules such as clhmc will not work as it needs a GMM, you may want to consider installing")
                print(__splt)
        else:
            print("%s\n \rFailed to import sklearn (SciKit-Learn) module!" % (__splt))
            print("To properly use all DATeS modules, please install sklearn\n%s\n" % (__splt))
            raise


def __test_compilers(ignore_non_essential=True, silent=False):
    """
    Test all python modules those should be imported
    """
    # 1- gfortran
    cmd = "which gfortran"
    proc = subprocess.Popen([cmd, "cat", "/etc/services"], stdout=subprocess.PIPE, shell=True)
    out, err = proc.communicate()
    if not os.path.isfile(out.strip()):  # gfortran is not accessible
        if ignore_non_essential:
            if silent:
                pass
            else:
                print("%s\n \rCouldn't find gfortran on your machine!" % (__splt))
                print("Some modules indluding FATODE time integration will not work, you may want to consider gfortran")
                print(__splt)
        else:
            print("%s\n \rCouldn't find gfortran on your machine!" % (__splt))
            print("To properly use all DATeS modules, please install gfortran\n%s\n" % (__splt))
            raise IOError
    else:
        pass


def test_dates_dependecy(python_modules=True, compilers=True, ignore_non_essential=True, silent=False):
    """
    """
    if python_modules:
        __test_python_modules(ignore_non_essential=ignore_non_essential, silent=silent)
    if compilers:
        __test_compilers(ignore_non_essential=ignore_non_essential, silent=silent)
