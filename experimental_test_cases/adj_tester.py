
# Define environment variables and update Python search path;
# this is a necessary call that must be inserted in the beginning of any driver.
import dates_setup
dates_setup.initialize_dates(random_seed=0)
#

#
# import fatode_erk_fwd  # pre-built wrapper
import erkadj_setup  # wrapper built on the fly

erkadj_setup.create_wrapper(verbose=True)
import erkadj
from fatode_erk_adjoint import FatODE_ERK_ADJ
import numpy as np

# import dates_setup
# dates_setup.initialize_dates(random_seed=0)
#
import dates_utility as utility  # import DATeS utility module(s)
from lorenz_models import Lorenz96  as Lorenz

import functools

import numpy.random as nr


def fun(t, y, f, n):
    """
    """
    # Do not modify Fortran memory as a whole,
    # modify at element level
    for i in range(n):
        f[i] = y[i]

def jac(t, y, fjac, n):
    """
    """
    # Do not modify Fortran memory as a whole,
    # modify at element level
    for i in range(n):
        for j in range(n):
            fjac[i,j] = 0.0
        fjac[i,i] = 1.0

def adjinit(t, y, lambda_, n, nadj):
    """
    """
    # Do not modify Fortran memory as a whole,
    # modify at element level
    for i in range(n):
        lambda_[i] = lambda_[i]

def jacp(t, y, fpjac, n, np):  # n,np,t,y,fpjac
    """
    """
    fpjac = np.zeros((n, np), order='F')


def drdy(t, y, nadj, n, nr):
    """
    """
    ry = np.zeros((nr, nadj), order='F')
    return ry


def drdp(t, y, rp, nadj, n, nr):  # nadj,n,nr,t,y,rp
    """
    """
    rp = np.zeros((nr, nadj), order='F')


def qfun(t, y, r, n, nr):
    """
    """
    r = np.zeros(nr)



if __name__ == '__main__':
    #
    model_configs = dict(adjoint_integrator_scheme='ADJ-ERK',
                         create_background_errors_correlations=True
                         )
    model = Lorenz(model_configs=model_configs)

    # set dummy values
    nvar = 40
    nadj = 1

    y = model.state_vector(nr.rand(nvar))
    print y
    lambda_ = model.state_vector(np.ones(nvar))
    tin = 0.0
    tout = 1.0
    atol = np.ones(nvar)*1e-5
    rtol = np.ones(nvar)*1e-7
    icntrl_u = np.zeros(20, dtype=np.int32)
    rcntrl_u = np.zeros(20)
    istatus_u = np.zeros(20, dtype=np.int32)
    rstatus_u = np.zeros(20)
    ierr_u = np.zeros(1, dtype=np.int32)

    # Create non_sparse jacobian
    jac_handle = functools.partial(model.step_forward_function_Jacobian, create_sparse=False)
    
    # create the adjoint integrator instance
    options_dict = {'y':y,                     # in/output rank-1 array('d') with bounds (nvar)
                    'model':model,
                    'lambda_':lambda_,             # (we may need other name!) in/output rank-2 array('d') with bounds (nvar,nadj)
                    'tin':tin,                   # input float
                    'tout':tout,                   # input float
                    'atol':atol,                  # input rank-1 array('d') with bounds (nvar)
                    'rtol':rtol,                  # input rank-1 array('d') with bounds (nvar)
                    'fun':model.step_forward_function,                    # call-back function
                    'jac':jac_handle,                    # call-back function
                    'adjinit':adjinit,            # call-back function
                    'nvar':nvar,                  # input int, optional Default: len(y)
                    'np':None,                    # input int, optional Default: shape(mu,0)
                    'nadj':nadj,                  # input int, optional Default: shape(lambda_,1)
                    'nnz':None,                   # input int
                    'fun_extra_args':tuple(),     # input tuple, optional Default: ()
                    'jac_extra_args':tuple(),     # input tuple, optional Default: ()
                    'adjinit_extra_args':tuple(), # input tuple, optional Default: ()
                    'icntrl_u':icntrl_u,              # input rank-1 array('i') with bounds (20)
                    'rcntrl_u':rcntrl_u,              # input rank-1 array('d') with bounds (20)
                    'istatus_u':istatus_u,             # input rank-1 array('i') with bounds (20)
                    'rstatus_u':rstatus_u,             # input rank-1 array('d') with bounds (20)
                    'ierr_u':ierr_u,                # input int
                    'mu':None,                    # in/output rank-2 array('d') with bounds (np,nadj)
                    'jacp':jacp,                  # call-back function
                    'jacp_extra_args':tuple(),    # input tuple, optional Default: ()
                    'drdy':drdy,                  # call-back function
                    'drdy_extra_args':tuple(),    # input tuple, optional Default: ()
                    'drdp':drdp,                  # call-back function
                    'drdp_extra_args':tuple(),    # input tuple, optional Default: ()
                    'qfun':qfun,                  # call-back function
                    'qfun_extra_args':tuple(),    # input tuple, optional Default: ()
                    'q':None,                     # in/output rank-1 array('d') with bounds (nadj)
                    }

    adjoint_integrator = FatODE_ERK_ADJ( options_dict=options_dict)

    # adjoint_integrator = erkadj.erk_adj_f90_integrator

    # call the adjoint integrator
    lambda_ = adjoint_integrator.integrate_adj(y=y, lambda_=lambda_, tin=tin, tout=tout, atol=atol, rtol=rtol)

    print lambda_
    
