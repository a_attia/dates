#!/usr/bin/env python

"""
============================================================================================
=                                                                                          =
=   DATeS: Data Assimilation Testing Suite.                                                =
=                                                                                          =
=   Copyright (C) 2016  A. Sandu, A. Attia, P. Tranquilli, S.R. Glandon,                   =
=   M. Narayanamurthi, A. Sarshar, Computational Science Laboratory (CSL), Virginia Tech.  =
=                                                                                          =
=   Website: http://csl.cs.vt.edu/                                                         =
=   Phone: 540-231-6186                                                                    =
=                                                                                          =
=   This program is subject to the terms of the Virginia Tech Non-Commercial/Commercial    =
=   License. Using the software constitutes an implicit agreement with the terms of the    =
=   license. You should have received a copy of the Virginia Tech Non-Commercial License   =
=   with this program; if not, please contact the computational Science Laboratory to      =
=   obtain it.                                                                             =
=                                                                                          =
============================================================================================
"""

import sys
import numpy as np

_random_seed = 2345


# Define environment variables and update Python search path;
# this is a necessary call that must be inserted in the beginning of any driver.
import dates_setup
dates_setup.initialize_dates(random_seed=_random_seed)
#
import dates_utility as utility  # import DATeS utility module(s)


# Create a model object
# ---------------------
from lorenz_models import Lorenz96  as Lorenz
model_configs = dict(adjoint_integrator_scheme='ERK',
                     create_background_errors_correlations=True,
                     )
model = Lorenz(model_configs=model_configs)
#
obs_checkpoints = np.arange(0.1,3.001, 0.1)
da_checkpoints = np.arange(0, 3.001, 0.6)
analysis_trajectory_timespan = np.arange(0, 3.001, 0.1)
#
#

# Create DA pieces:
# ---------------------
# this includes:
#   i-   forecast trajectory/state
#   ii-  initial ensemble,
#   iii- filter/smoother/hybrid object.
#
# create initial ensemble...
ensemble_size = 25
initial_ensemble = model.create_initial_ensemble(ensemble_size=ensemble_size)
ref_IC = model._reference_initial_condition.copy()

# create smoother object
from hmc_smoother import HMCSmoother as SMOOTHER
smoother_configs = dict(model=model,
                        reference_time=analysis_trajectory_timespan[0],
                        reference_state=ref_IC,
                        analysis_ensemble=initial_ensemble,
                        forecast_ensemble=None,
                        ensemble_size=ensemble_size,
                        chain_parameters=dict(Symplectic_integrator='2-stage',
                                             Hamiltonian_num_steps=10,
                                             Hamiltonian_step_size=0.01,
                                             Mass_matrix_type='prior_precisions',
                                             Burn_in_num_steps=30,
                                             Mixing_steps=10,
                                             ),
                       prior_distribution='gaussian',
                       gmm_prior_settings=dict(clustering_model='gmm',
                                               cov_type=None,
                                               localize_covariances=False,
                                               inf_criteria='bic',
                                               number_of_components=None,
                                               min_number_of_components=None,
                                               max_number_of_components=None,
                                               min_number_of_points_per_component=3,
                                               invert_uncertainty_param=False,
                                               approximate_covariances_from_comp=False,
                                               use_oringinal_hmc_for_one_comp=True,
                                               initialize_chain_strategy='forecast_mean'  #  highest_weight or 'forecast_mean'
                                               ),
                        inflation_factor=1.0,
                        obs_covariance_scaling_factor=1.0,
                        obs_adaptive_prescreening_factor=None,
                        localize_covariances=True,
                        localization_method='covariance_filtering',
                        localization_radius=4,
                        localization_function='gauss',
                        forecast_inflation_factor=1.0,
                        analysis_inflation_factor=1.0,
                        hybrid_background_coeff=1.0  # this is multiplied by the modeled background error covariance matrix
                       )

output_configs = dict(scr_output=True,
                      file_output=False, # this will be overridden by the process anyways.
                      file_output_moment_only=False,
                      file_output_moment_name='mean',
                      verbose=False
                      )


smoother_obj = SMOOTHER(smoother_configs=smoother_configs, output_configs=output_configs)

# Create sequential DA
# processing object:
# ---------------------
# Here this is a filtering_process object;
from smoothing_process import SmoothingProcess
experiment_configs = dict(smoother=smoother_obj,
                          experiment_timespan=[analysis_trajectory_timespan[0], analysis_trajectory_timespan[-1]],
                          obs_checkpoints=obs_checkpoints,
                          observations_list=None,
                          da_checkpoints=da_checkpoints,
                          initial_ensemble=initial_ensemble,
                          ref_initial_condition=ref_IC,
                          ref_initial_time=analysis_trajectory_timespan[0],
                          analysis_timespan=analysis_trajectory_timespan,
                          # random_seed=_random_seed
                          )
experiment = SmoothingProcess(assimilation_configs=experiment_configs, output_configs={'file_output':True,'verbose':True})

#
# print("Terminated per request...")
# sys.exit()

# run the sequential filtering over the timespan created by da_checkpoints
experiment.recursive_assimilation_process()

#
# Clean executables and temporary modules
# ---------------------
utility.clean_executable_files()
#
